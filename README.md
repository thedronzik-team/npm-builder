Simple image for run npm task

# About
- this image included:
    - alpine:3.4
    - node:7.10
- on run image automaticaly update npm dependency
- workdir - `/app`
- entry point of this image is `npm <command>`

 
# Usage

- mount project directory with file `package.json` into `/app`
- set `<command>` equal script name in your `package.json`

# Example `package.json` file
```
{
  "name": "exaple",
  "version": "1.0.0",
  "scripts": {
    "serv": "webpack-dev-server",
    "release": "webpack --release",
    "build": "webpack --build"
  },
  "dependencies": {...}

```
# Example `docker-compose.yml` file
```
version: '3'

volumes:
  bundles:

services:

  npm-builder:
    image: thedronzik/npm-builder
    expose:
      - "9000" # webpack
    cache:
      paths:
        - node_modules/
    volumes:
      - .:/app
      - bundles:/app/bundles # output directory (for local WATCH mode)
    artifacts:
      paths:
        - bundles # output artifact for next stage (for CI mode)
    command: ["npm run serv"]
    environment:
      - HOSTNAME=localhost
```