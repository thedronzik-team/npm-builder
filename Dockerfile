# Set the base image
FROM node:alpine

# File Author / Maintainer
MAINTAINER thedronzik <thedronzik@gmail.com>

# Define working directory
RUN mkdir /app
RUN apk --update add git openssh && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*

WORKDIR /app
ENV APP_PATH /app

# выполняются в дочернем Dockerfile, при использовании в .gitlab-ci.yml как "image: thedronzik/npm-builder:latest"
# эти строки будут проигнорированы
#ONBUILD ADD . /app
#ONBUILD RUN npm install

# Add a /app volume "/app/node_modules",
VOLUME ["/home/dev/.npm"]

# Add startup script
#ADD ./start.sh /app/start.sh
#RUN chmod 755 /app/start.sh

RUN node -v && npm -v

# Entry point with npm install and execute "npm <parameters>" from command configuration array
#ENTRYPOINT ["/app/start.sh"]
#CMD ["/bin/sh", "/start.sh"]
