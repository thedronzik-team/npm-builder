#!/usr/bin/env sh
echo "ls ./"
ls ./
echo "ls ../"
ls ../
if [ $APP_PATH ]
then
	cd $APP_PATH
fi

echo "npm install..."
npm install

if [ $# -ge 0 ]
then
	echo $@ "..."
	$@
fi